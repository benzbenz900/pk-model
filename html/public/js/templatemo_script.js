var menuDisabled = false;
new LazyLoad()
$(() => {
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 170,
        itemMargin: 5,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function (slider) {
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('#main-wrapper').delay(350).css({ 'overflow': 'visible' });
        }
    });

    if ($(window).width() > 767) {
        var navWidth = $('.navbar .navbar-nav').width();

        $('hgroup').css("maxWidth", navWidth + "px");
        $('.templatemo-content').css("maxWidth", navWidth + "px");
        $('.footer-wrapper').css("maxWidth", navWidth + "px");
    }

    $('.site-name').click(function () {
        location.reload();
    });

    var defaultImgSrc = $('img.main-img').attr('data-src');
    $.backstretch(defaultImgSrc, { speed: 400 });

    $(".nav a").on('click', function (e) {
        if ($(this).hasClass("external")) {
            return;
        }
        e.preventDefault();
        if (menuDisabled == false) // check the menu is disabled?
        {
            menuDisabled = true; // disable the menu

            var name = $(this).attr('href');
            $('.nav li').removeClass('active');

            var menuClass = $(this).parent('li'); // .attr('class')
            $(menuClass).addClass('active');

            // get image url and assign to backstretch for background
            var imgSrc = $("img" + name + "-img").attr('data-src');
            $.backstretch(imgSrc, { speed: 400 }); //backstretch for background fade in/out

            // content slide in/out
            $("section.active").animate({ left: -$("section.active").outerWidth() }, 300, function () {
                $(this).removeClass("active");
                $(this).hide();
                $(name + "-text").removeClass("inactive");
                $(name + "-text").css({ left: '703px', top: '0px' });
                $(name + "-text").show();
                $(name + "-text").animate({ left: '0px' }, 300, function () {
                    $(this).addClass("active");

                    $.backstretch("resize");
                    $(window).resize();

                    menuDisabled = false;
                });
            });
        }
        return;
    });

    if ('loading' in HTMLImageElement.prototype) {
        const images = document.querySelectorAll('img[loading="lazy"]');
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    }

})
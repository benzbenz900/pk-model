// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDJa00lmkaye2L6t1UldoG7rfJx7aK0IaA",
    authDomain: "pkmodelstudio.firebaseapp.com",
    projectId: "pkmodelstudio",
    storageBucket: "pkmodelstudio.appspot.com",
    messagingSenderId: "311201991611",
    appId: "1:311201991611:web:290073d9acc5a7ca44129e",
    measurementId: "G-JM18TPVSNG"
};

firebase.initializeApp(firebaseConfig);

// Get a reference to the Firestore database service
const db = firebase.firestore();

// Get a reference to the Firebase Storage service
const storage = firebase.storage();

let today = new Date();
today.setHours(today.getHours() + 7); // add 7 hours for timezone +7
let formattedDate = today.toJSON().slice(0, 10);
// Get references to HTML elements
const addForm = document.getElementById("addForm");
const tableBody = document.querySelector("#table tbody");
const addModal = document.getElementById("addModal");
const editModal = document.getElementById("editModal");
const editForm = document.getElementById("editForm");
const editId = document.getElementById("editId");
const editName = document.getElementById("editName");
const editDescription = document.getElementById("editDescription");
const editDate = document.getElementById("editDate");
const editFile = document.getElementById("editFile");
const addButton = document.getElementById("addButton");
const closeImageShow = document.getElementById("closeImageShow");
const imageModal = document.getElementById("imageModal");
const showImageList = document.getElementById("showImageList");
const closeeditModal = document.getElementById("closeeditModal");
const closeaddModal = document.getElementById("closeaddModal");
const adddate = document.getElementById("date");

adddate.setAttribute("max", formattedDate);
adddate.setAttribute("value", formattedDate);
editDate.setAttribute("max", formattedDate);
// Define a function to get a download URL for a file in Firebase Storage
async function getDownloadURL(ref) {
    const url = await ref.getDownloadURL();
    return url;
}
ClassicEditor.defaultConfig={toolbar:{items:["heading","|","bold","italic","link","bulletedList","numberedList","|","indent","outdent","|","imageUpload","blockQuote","mediaEmbed","undo","redo"]},language:"th"}
ClassicEditor.create( document.querySelector( 'textarea' ) )
                                .then( editor => {
                                        console.log( editor );
                                } )
                                .catch( error => {
                                        console.error( error );
                                } );

// Define a function to render a row in the table
function renderRow(doc,index) {
    const row = document.createElement("tr");

    // สร้างเซลล์แสดงชื่อผลงาน
    const IDCell = document.createElement("td");
    IDCell.textContent = index;
    row.appendChild(IDCell);

    // สร้างเซลล์แสดงชื่อผลงาน
    const nameCell = document.createElement("td");
    nameCell.textContent = doc.data().name;
    row.appendChild(nameCell);

    // สร้างเซลล์แสดงรายละเอียด
    const descriptionCell = document.createElement("td");
    descriptionCell.textContent = doc.data().description.replace(/(<([^>]+)>)/ig, "").substring(0, 100);
    row.appendChild(descriptionCell);

    // สร้างเซลล์แสดงวันที่
    const dateCell = document.createElement("td");
    dateCell.textContent = doc.data().date;
    row.appendChild(dateCell);

    // สร้างเซลล์แสดงรูปภาพ
    const imageCell = document.createElement("td");
    const showImageButton = document.createElement("button");
    showImageButton.innerHTML = "&#127889; แสดงรูป";
    showImageButton.className = "preview";
    showImageButton.addEventListener("click", () => {
        showImageList.innerHTML = "";
        imageModal.style.display = "block";
        const images = doc.data().images;
        images.forEach(async (image) => {
            const imageRef = storage.ref(image.path);
            const url = await getDownloadURL(imageRef);
            const img = document.createElement("img");
            img.src = url;
            img.width = 100;
            showImageList.appendChild(img);
        });
    });
    imageCell.appendChild(showImageButton);
    row.appendChild(imageCell);

    // สร้างเซลล์แสดงปุ่มแก้ไข
    const editCell = document.createElement("td");
    const editButton = document.createElement("button");
    editButton.innerHTML = "&#9998; แก้ไข";
    editButton.className = "edit";
    editButton.addEventListener("click", () => {
        editModal.style.display = "block";
        editId.value = doc.id;
        editName.value = doc.data().name;
        editDescription.value = doc.data().description;
        editDate.value = doc.data().date;
    });
    editCell.appendChild(editButton);
    row.appendChild(editCell);

    // สร้างเซลล์แสดงปุ่มลบ
    const deleteCell = document.createElement("td");
    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = "&#128465; ลบ";
    deleteButton.className  = "delete";
    deleteButton.addEventListener("click", async () => {
        const docRef = db.collection("projects").doc(doc.id);

        // Delete all old images from Firebase Storage and Firestore
        const oldImages = doc.data().images;
        // Delete all old images from Firebase Storage and Firestore
        if (oldImages.length > 0) {
            for (let i = 0; i < oldImages.length; i++) {
                const image = oldImages[i];
                const imageRef = storage.ref(image.path);
                await imageRef.delete();
            }
        }
        await docRef.delete();
        renderTable();
    });
    deleteCell.appendChild(deleteButton);
    row.appendChild(deleteCell);

    // เพิ่มแถวลงในตาราง
    tableBody.appendChild(row);
}

// Define a function to render all projects in the table
function renderTable() {
    // Clear the table body
    tableBody.innerHTML = "";

    // Get all projects from Firestore
    db.collection("projects")
        .limit(20)
        .get()
        .then((querySnapshot) => {
            let index = 1
            querySnapshot.forEach((doc) => {
                renderRow(doc,index);
                index++
            });
        });
}

// Add a listener to the add form submit event
addForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    // Get the form data
    const name = addForm.name.value;
    const description = addForm.description.value;
    const date = addForm.date.value;
    const images = addForm.file.files;

    // Create a new project in Firestore
    const docRef = await db.collection("projects").add({
        name,
        description,
        date,
        images: [],
    });

    // Upload the images to Firebase Storage
    if (images.length > 0) {
        for (let i = 0; i < images.length; i++) {
            const image = images[i];
            const path = `images/${docRef.id}/${image.name}`;
            const imageRef = storage.ref(path);
            const snapshot = await imageRef.put(image);
            const url = await snapshot.ref.getDownloadURL();
            await docRef.update({
                images: firebase.firestore.FieldValue.arrayUnion({
                    name: image.name,
                    path,
                    url,
                }),
            });
        }
    }

    // Clear the form
    addForm.reset();

    // Hide the add modal
    addModal.style.display = "none";

    // Render the updated table
    renderTable();
});

// Add a listener to the edit form submit event
editForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    // Get the form data
    const id = editId.value;
    const name = editName.value;
    const description = editDescription.value;
    const date = editDate.value;
    const images = editForm.editFile.files;

    // Update the project in Firestore
    const docRef = db.collection("projects").doc(id);
    await docRef.update({
        name,
        description,
        date,
    });

    // Delete all old images from Firebase Storage and Firestore
    const oldImages = await docRef.get().then((doc) => doc.data().images);
    if (oldImages.length > 0) {
        for (let i = 0; i < oldImages.length; i++) {
            const image = oldImages[i];
            const imageRef = storage.ref(image.path);
            await imageRef.delete();
        }
    }
    await docRef.update({
        images: [],
    });

    // Upload the new images to Firebase Storage and Firestore
    if (images.length > 0) {
        for (let i = 0; i < images.length; i++) {
            const image = images[i];
            const path = `images/${id}/${image.name}`;
            const imageRef = storage.ref(path);
            const snapshot = await imageRef.put(image);
            const url = await snapshot.ref.getDownloadURL();
            await docRef.update({
                images: firebase.firestore.FieldValue.arrayUnion({
                    name: image.name,
                    path,
                    url,
                }),
            });
        }
    }

    // Hide the edit modal
    editModal.style.display = "none";

    // Render the updated table
    renderTable();
});

addButton.addEventListener("click", () => {
    addModal.style.display = "block";
});
closeImageShow.addEventListener("click", () => {
    imageModal.style.display = "none";
});
closeaddModal.addEventListener("click", () => {
    addModal.style.display = "none";
});
closeeditModal.addEventListener("click", () => {
    editModal.style.display = "none";
});

// Render the initial table
renderTable();
imageModal.style.display = "none";
addModal.style.display = "none";
editModal.style.display = "none";
export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'รับทำโมเดล เครื่องบิน เรือ PK Aircraft & Ship Studio - PK Model by PKModelStudio.com',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: "msapplication-TileColor", content: "#da532c" },
      { name: "theme-color", content: "#ffffff" },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'PK Model โมเดลเครื่องบิน (เครื่องบินจำลอง) และ โมเดลเรือ (เรือจำลอง) มากมายหลายแบบ อาทิเช่น เครื่องบินโดยสาร เครื่องบินฝึก เครื่องบินพลเรือน แบบต่างๆ เรือบรรทุกสินค้า เรือพาณิชย์ เรือรบแบบต่างๆ  ตามแบบ ตามสั่ง ' }
    ],
    link: [
      { rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png" },
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png" },
      { rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png" },
      { rel: "manifest", href: "/site.webmanifest" },
      { rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#5bbad5" },
      { rel: "stylesheet", href: "css/templatemo_main.css" },
      { rel: "stylesheet", href: "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" }
    ],
    script: [
      { src: "js/jquery.min.js" },
      { src: "js/jquery.backstretch.min.js" },
      { src: "js/jquery.flexslider.min.js" },
      { src: "__/firebase/8.3.1/firebase-app.js" },
      { src: "__/firebase/8.3.1/firebase-analytics.js" },
      { src: "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js" },
      { src: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.1/dist/lazyload.min.js" }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    ['@nuxtjs/google-analytics', {
      id: '266840832'
    }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // 'bootstrap-vue/nuxt',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  generate: {
    dir: 'html/public',
    fallback: '200.html'
  },
  build: {
    publicPath: 'https://pkmodelstudio.web.app/',
  },
}
